import requests
import os
import re
from datetime import datetime
from time import sleep
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
from subprocess import call



GITLAB_URL = 'http://gitlab'
USERNAME = os.environ['GITLAB_USERNAME']
PASSWORD = os.environ['GITLAB_PASSWORD']



# wait for gitlab health check
HEALTH_CHECK_URL = GITLAB_URL + '/-/readiness'

ready = False
while ready == False:
    print(datetime.now(), 'waiting for gitlab')
    try:
        resp = requests.get(HEALTH_CHECK_URL, timeout=5).json()
        ready = True
        for key, val in resp.items():
            if val['status'] != 'ok':
                ready = False
                break
    except:
        pass
    sleep(5)


# access login page
jar = requests.cookies.RequestsCookieJar()
ses = requests.Session()
r = ses.get(f'{GITLAB_URL}/users/sign_in', cookies=jar)

# get authenticity token
auth_token = re.search('csrf-token" content="(.*)"', r.text).group(1)

# login
data = {'user[login]': USERNAME, 'user[password]': PASSWORD, \
        'authenticity_token': auth_token}
r = ses.post(f'{GITLAB_URL}/users/sign_in', data=data, cookies=jar)



# create project
r = ses.get(f'{GITLAB_URL}/projects/new', cookies=jar)
auth_token = re.search('csrf-token" content="(.*)"', r.text).group(1)
data = {'utf8': '✓', \
        'authenticity_token': auth_token, \
        'project[ci_cd_only]': 'false', \
        'project[name]': 'repo', \
        'project[namespace_id]': '1', \
        'project[path]': 'repo', \
        'project[description]': '', \
        'project[visibility_level]': '0'}
r = ses.post(f'{GITLAB_URL}/projects', data=data, cookies=jar)



# push sample repo
repo_path='/repo'
call(['git', 'init', '.'], cwd=repo_path)
call(['git', 'remote', 'add', 'origin', 'http://root:rootpassword@localhost:8001/root/repo.git'], cwd=repo_path)
call(['git', 'remote', 'add', 'gitlab', 'http://root:rootpassword@gitlab/root/repo.git'], cwd=repo_path)
call(['git', 'add', '.'], cwd=repo_path)
call(['git', 'commit', '-m', 'init'], cwd=repo_path)
call(['git', 'push', 'gitlab', 'master'], cwd=repo_path)



# get runner registration token
r = ses.get(f'{GITLAB_URL}/admin/runners', cookies=jar)
reg_token = re.search('id="registration_token">(.*)<', r.text).group(1)

class HTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        d = {'registration_token': reg_token}
        self.wfile.write(json.dumps(d).encode('utf-8'))

print('Serving at 8101')
httpd = HTTPServer(('0.0.0.0', 80), HTTPRequestHandler)
httpd.serve_forever()
